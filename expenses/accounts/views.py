from django.shortcuts import render, redirect
from .forms import LoginForm, SignUpForm
from django.contrib.auth import logout, login, authenticate


# Create your views here.
def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST):
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request,user)
                return redirect('home')
            else:
                pass

    else:
        form = LoginForm()

    return render(request, 'login.html', {'form': form})

def logout(request):
    logout(request)
    return redirect('login')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data.get('password')
            password_confirmation = form.cleaned_data.get('password_confirmation')
            if password == password_confirmation:
                user = form.save()
                user = authenticate(username=user.username, password = password)
                login(request, user)
                return redirect('receipt_list')
            else:
                form.add_error('password_confirmation', 'The passwords do not match.')
    else:
        form = SignUpFormForm()
        return redner(request, 'registration/signup.html', {'form': form})
    else:
        form = SignUpForm()
    return render(request, 'accounts/signup.html', {'form': form})
