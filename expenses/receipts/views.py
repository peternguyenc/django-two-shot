from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCategoryForm

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipt_list.html', {'receipts': receipts})

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    return render(request, 'create_receipt.html', {'form': form})

def category_list(request):
    categories = ExpenseCategory.objects.annotate(num_receipts=models.Count('receipt'))
    return render(request, 'category_list.html', {'categories': categories})

def account_list(request):
    accounts = Account.objects.annotate(num_receipts=models:.Count('receipt'))
    return render(request, 'account_list.html', {'accounts': accounts})

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')  # Replace 'category_list' with the name of your category list view or URL pattern
    else:
        form = ExpenseCategoryForm()
    return render(request, 'create_category.html', {'form': form})
